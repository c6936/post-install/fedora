#!/bin/bash
work_dir=$(pwd)
mkdir -p ~/.config
mkdir -p ~/.config/fish
sudo mkdir -p /usr/share/xsessions

gpu_is_nvidia=n

while getopts ":n" arg; do
    case "${arg}" in
        n) gpu_is_nvidia=y
    esac
done

sudo dnf check-update
sudo dnf -y upgrade
PKG_LIST="./my_pkgs.txt"
LINES=$(cat $PKG_LIST)
for LINE in $LINES
do
    sudo dnf -y install "$LINE"
done

curl -sS https://starship.rs/install.sh | sh

wget -O mullvad.rpm --content-disposition https://mullvad.net/download/app/rpm/latest
sudo dnf install -y ./mullvad.rpm

git clone https://github.com/jonaburg/picom
cd picom
meson --buildtype=release . build
ninja -C build
sudo ninja -C build install
cd ..

cd ~/.config
git clone https://gitlab.com/c6936/st.git
cd st
sudo make install
cd $work_dir

cd ~
git clone https://gitlab.com/c6936/dwm.git
mv dwm .dwm
cd .dwm/dwm-6.4/
sudo make install
cd $work_dir
sudo cp ~/.dwm/dwm.desktop /usr/share/xsessions/

git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs.d
~/.emacs.d/bin/doom install
~/.emacs.d/bin/doom sync

git clone https://gitlab.com/c6936/shellrc.git
cd shellrc/
/bin/bash shellrc.sh -l lsd
cd ..

echo -e "Defaults\tenv_reset,pwfeedback" | sudo tee -a /etc/sudoers
sudo systemctl enable --now sshd
sudo systemctl enable --now syncthing@jon
echo "reboot this bitch"
